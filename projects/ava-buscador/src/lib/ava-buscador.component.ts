import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'if-ava-buscador',
  template: `
    <!-- Campo de input para buscar palabras -->
    <input type="text" (keydown.enter)="marcarTexto($event)" />
    <!-- Contenido original que está oculto -->
    <div #contenido [hidden]="true">
      <ng-content>
      </ng-content>
    </div>
    <!-- Contenido controlado con palabras subrayadas -->
    <div [innerHTML]="contenidoControlado">
    </div>
  `,
  styles: [`.textoMarcado {background-color: yellow}`],
  encapsulation: ViewEncapsulation.None
})
export class AvaBuscadorComponent implements OnInit {

  @ViewChild('contenido', { static: true }) content: ElementRef<any> | undefined;

  contenidoOriginal: string = '';
  contenidoControlado: string = ''; // texto original, pero con las palabras subrayadas

  constructor() { }

  ngOnInit(): void {
    // El texto original y el texto controlado son el texto que está dentro de "#contenido"
    this.contenidoOriginal = this.contenidoControlado = this.content!.nativeElement.textContent;
  }

  marcarTexto(event: any) {
    // Obtenemos lo que haya escrito en el input
    let textoBusqueda = event.target.value;
    this.contenidoControlado = this.contenidoOriginal;
    // Sustituimos las palabras encontradas por la misma palabra, pero con un span y una vclase de subrayado
    this.contenidoControlado = this.contenidoOriginal.replace(
      new RegExp(textoBusqueda, 'g'),
      `<span class="textoMarcado"> ${textoBusqueda}</span>`
    )
  }

}
