import { TestBed } from '@angular/core/testing';

import { AvaBuscadorService } from './ava-buscador.service';

describe('AvaBuscadorService', () => {
  let service: AvaBuscadorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AvaBuscadorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
