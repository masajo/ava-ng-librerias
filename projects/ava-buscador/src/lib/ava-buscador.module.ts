import { NgModule } from '@angular/core';
import { AvaBuscadorComponent } from './ava-buscador.component';



@NgModule({
  declarations: [
    AvaBuscadorComponent
  ],
  imports: [
  ],
  exports: [
    AvaBuscadorComponent
  ]
})
export class AvaBuscadorModule { }
