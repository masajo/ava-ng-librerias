import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvaBuscadorComponent } from './ava-buscador.component';

describe('AvaBuscadorComponent', () => {
  let component: AvaBuscadorComponent;
  let fixture: ComponentFixture<AvaBuscadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvaBuscadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvaBuscadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
