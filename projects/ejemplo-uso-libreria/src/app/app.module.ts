import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { AvaBuscadorModule } from 'ava-buscador';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AvaBuscadorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
